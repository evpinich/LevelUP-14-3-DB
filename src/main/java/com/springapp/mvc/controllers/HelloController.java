package com.springapp.mvc.controllers;

import com.springapp.mvc.services.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;




@Controller
@RequestMapping("/")
public class HelloController {
	@Autowired
	MainService mainService;
	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {



		model.addAttribute("message",mainService.getMainEntity());
		return "hello";
	}
}

