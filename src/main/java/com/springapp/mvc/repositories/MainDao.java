package com.springapp.mvc.repositories;

import com.springapp.mvc.domeins.MainEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by java on 14.06.2015.
 */

@Repository
public class MainDao {
    public MainEntity getMainEntity(){
        return new MainEntity();
    }

}
