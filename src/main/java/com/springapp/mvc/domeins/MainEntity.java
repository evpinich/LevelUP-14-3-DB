package com.springapp.mvc.domeins;

/**
 * Created by java on 14.06.2015.
 */
public class MainEntity {
    private String Name;
    private String SecondName;
    private String Age;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }

    public String getAgr() {
        return Age;
    }

    public void setAgr(String agr) {
        Age = agr;
    }
}
