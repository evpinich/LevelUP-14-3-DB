package com.springapp.mvc.services;

import com.springapp.mvc.domeins.MainEntity;
import com.springapp.mvc.repositories.MainDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by java on 14.06.2015.
 */
@Service
public class MainService {
    @Autowired
    MainDao mainDao;
    public MainEntity getMainEntity(){
        return  mainDao.getMainEntity();
    }



}
